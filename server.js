import express from 'express';
import 'babel-polyfill';
import cors from 'cors';
import env from './env.js';

import cpuRoute from './app/routes/cpuRoute';

const app = express();
const corsOptions = {
  origin: 'http://localhost:3000',
  allowedHeaders: ['X-Requested-With', 'content-type'],
  credentials: true,
}
app.use(cors(corsOptions));
app.use(express.urlencoded({ extended: false }));
app.use(express.json());

app.use('/api/v1', cpuRoute);

app.listen(env.port).on('listening', () => {
  console.log(`🚀 are live on ${env.port}`);
});


export default app;
