import express from 'express';

import { getCpuCores, createCoreRow, getCpuUsage, updateCpuUsage } from '../controllers/cpuController';
import { getRamUsage, updateRamUsage } from '../controllers/ramController';
import { getLogList } from '../controllers/logController';

const router = express.Router();

router.get('/cpu/cores', getCpuCores);
router.post('/cpu/cores', createCoreRow);

router.get('/cpu/usage', getCpuUsage);
router.put('/cpu/usage', updateCpuUsage)

router.get('/ram/usage', getRamUsage);
router.put('/ram/usage', updateRamUsage)

router.get('/log', getLogList)

export default router;
