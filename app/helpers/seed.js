import pool from '../db/dev/pool';
import moment from "moment";

pool.on('connect', () => {
  console.log('connected to the db');
});

const getRandomUsage = () => Math.floor(Math.random() * Math.floor(100))

const getRandomText = () => {
    const length = Math.floor(Math.random() * Math.floor(50))
    const randomChars = ' ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    let result = '';
    for ( let i = 0; i < length; i++ ) {
        result += randomChars.charAt(Math.floor(Math.random() * randomChars.length));
    }
    return result;
}

const seedCpuCores = () => {
  const seedCpuCoresQuery = `INSERT INTO public.cpu_cores(id, cpu_core, usage) VALUES 
  ( default, 'Core 1', '${getRandomUsage()}'),
  ( default, 'Core 2', '${getRandomUsage()}'),
  ( default, 'Core 3', '${getRandomUsage()}'),
  ( default, 'Core 4', '${getRandomUsage()}'),
  ( default, 'Core 5', '${getRandomUsage()}'),
  ( default, 'Core 6', '${getRandomUsage()}'),
  ( default, 'Core 7', '${getRandomUsage()}'),
  ( default, 'Core 8', '${getRandomUsage()}')
  `;

  pool.query(seedCpuCoresQuery)
    .then((res) => {
      console.log(res);
      pool.end();
    })
    .catch((err) => {
      console.log(err);
      pool.end();
    });
};


const seedLogList = () => {
    let timeToDb = moment(new Date()).startOf('day')
    const dayEnd = moment(new Date()).endOf('day')
    const getRows = () => {
        const rows = []
        while (timeToDb < dayEnd) {
            rows.push(`(default, '${getRandomText()}', '${timeToDb.format()}'::timestamp with time zone)`)
            timeToDb.add(10, 'minute')
        }
        return rows.join(',')
    }

    const seedCpuCoresQuery = `INSERT INTO public.log_list(id, log, date) VALUES ${getRows()};`;


    pool.query(seedCpuCoresQuery)
        .then((res) => {
            console.log(res);
            pool.end();
        })
        .catch((err) => {
            console.log(err);
            pool.end();
        });
};

const seedCpuUsage = () => {
    let timeToDb = moment(new Date()).startOf('day')
    const dayEnd = moment(new Date()).endOf('day')

    const getRows = () => {
        const rows = []
        while (timeToDb < dayEnd) {
            rows.push(`(default, ${getRandomUsage()}, '${timeToDb.format()}'::timestamp with time zone)`)
            timeToDb.add(1, 'minute')
        }
        return rows.join(',')
    }
    const seedCpuCoresQuery = `INSERT INTO public.cpu_usage(id, usage, date) VALUES ${getRows()};`;


    pool.query(seedCpuCoresQuery)
      .then((res) => {
        console.log(res);
        pool.end();
      })
      .catch((err) => {
        console.log(err);
        pool.end();
      });
};

const seedRamUsage = () => {
    let timeToDb = moment(new Date()).startOf('day')
    const dayEnd = moment(new Date()).endOf('day')

    const getRows = () => {
        const rows = []
        while (timeToDb < dayEnd) {
            rows.push(`(default, ${getRandomUsage()}, '${timeToDb.format()}'::timestamp with time zone )`)
            timeToDb.add(1, 'minute')
        }
        return rows.join(',')
    }
    const seedCpuCoresQuery = `INSERT INTO public.ram_usage(id, usage, date) VALUES ${getRows()};`;


    pool.query(seedCpuCoresQuery)
        .then((res) => {
            console.log(res);
            pool.end();
        })
        .catch((err) => {
            console.log(err);
            pool.end();
        });
};

const seedData = () => {
  seedCpuCores();
  seedCpuUsage();
  seedRamUsage();
  seedLogList();
};


pool.on('remove', () => {
  console.log('client removed');
  process.exit(0);
});

export { seedData }

require('make-runnable');
