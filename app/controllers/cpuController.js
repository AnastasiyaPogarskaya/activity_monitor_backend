import dbQuery from '../db/dev/dbQuery';

import {
	errorMessage, successMessage, status,
} from '../helpers/status';
import {empty} from "../helpers/validations";
import moment from "moment";

const getCpuCores = async (req, res) => {
	const getCpuWorkloadQuery = 'SELECT id, cpu_core, usage FROM public.cpu_cores';
	try {
		const { rows } = await dbQuery.query(getCpuWorkloadQuery);
		const dbResponse = rows;
		if (dbResponse[0] === undefined) {
			errorMessage.error = 'There are no logs';
			return res.status(status.bad).send(errorMessage);
		}
		successMessage.data = dbResponse;
		return res.status(status.success).send(successMessage);
	} catch (error) {
		errorMessage.error = 'An error Occured';
		return res.status(status.error).send(errorMessage);
	}
}

const createCoreRow = async (req, res) => {
	const {
		cpu_core, workload,
	} = req.body

	if (empty(cpu_core)) {
		errorMessage.error = 'Trip is required';
		return res.status(status.bad).send(errorMessage);
	}

	const createCoreQuery = `INSERT INTO
          public.cpu_cores(cpu_core, workload)
          VALUES($1, $2)
          returning *`;

	const values = [
		cpu_core,
		workload
	];

	try {
		const { rows } = await dbQuery.query(createCoreQuery, values);
		const dbResponse = rows[0];
		successMessage.data = dbResponse;
		return res.status(status.created).send(successMessage);
	} catch (error) {
		if (error.routine === '_bt_check_unique') {
			errorMessage.error = 'Seat Number is taken already';
			return res.status(status.conflict).send(errorMessage);
		}
		errorMessage.error = 'Unable to create core';
		return res.status(status.error).send(errorMessage);
	}
}


const getCpuUsage = async (req, res) => {
	const start = `'${moment(new Date()).add(-2, 'hour').format()}'`
	const end = `'${moment(new Date()).format()}'`

	const getCpuWorkloadQuery = `SELECT * FROM public.cpu_usage WHERE date >= ${start} AND date <= ${end}`;
	try {
		const { rows } = await dbQuery.query(getCpuWorkloadQuery);
		const dbResponse = rows;
		if (dbResponse[0] === undefined) {
			errorMessage.error = 'There are no logs';
			return res.status(status.bad).send(errorMessage);
		}
		successMessage.data = dbResponse;
		return res.status(status.success).send(successMessage);
	} catch (error) {
		errorMessage.error = 'An error Occured';
		return res.status(status.error).send(errorMessage);
	}
}

const updateCpuUsage = async (req, res) => {
	const { date, usage } = req.body;

	const dateQuery = `'${moment(date).format()}'`

	if (empty(usage)) {
		errorMessage.error = 'Usage is needed';
		return res.status(status.bad).send(errorMessage);
	}
	const findUsageQuery = 'SELECT * FROM public.cpu_usage WHERE date = $1';
	const updateUsageQuery = `UPDATE public.cpu_usage
        SET usage=$2 WHERE date=$1 returning id, usage, to_char(date, 'YYYY-MM-DD HH24:MI:SS TZ') as date`;
	try {
		const { rows } = await dbQuery.query(findUsageQuery, [dateQuery]);
		const dbResponse = rows[0];
		if (!dbResponse) {
			errorMessage.error = 'Usage Cannot be found';
			return res.status(status.notfound).send(errorMessage);
		}
		const response = await dbQuery.query(updateUsageQuery, [dateQuery, usage]);
		const dbResult = response.rows[0];
		delete dbResult.password;
		console.log(dbResult)
		successMessage.data = dbResult;
		return res.status(status.success).send(successMessage);
	} catch (error) {
		return res.status(status.error).send(errorMessage);
	}
}

export {
	getCpuCores,
	createCoreRow,
	getCpuUsage,
	updateCpuUsage
}
