import dbQuery from '../db/dev/dbQuery';

import {
	errorMessage, successMessage, status,
} from '../helpers/status';
import {empty} from "../helpers/validations";
import moment from "moment";

const getRamUsage = async (req, res) => {
	const start = `'${moment(new Date()).add(-2, 'hour').format()}'`
	const end = `'${moment(new Date()).format()}'`

	const getCpuWorkloadQuery = `SELECT * FROM public.ram_usage WHERE date >= ${start} AND date <= ${end}`;
	try {
		const { rows } = await dbQuery.query(getCpuWorkloadQuery);
		const dbResponse = rows;
		if (dbResponse[0] === undefined) {
			errorMessage.error = 'There are no logs';
			return res.status(status.bad).send(errorMessage);
		}
		successMessage.data = dbResponse;
		return res.status(status.success).send(successMessage);
	} catch (error) {
		errorMessage.error = 'An error Occured';
		return res.status(status.error).send(errorMessage);
	}
}

const updateRamUsage = async (req, res) => {
	const { date, usage } = req.body;

	const dateQuery = `'${moment(date).format()}'`

	if (empty(usage)) {
		errorMessage.error = 'Usage is needed';
		return res.status(status.bad).send(errorMessage);
	}
	const findUsageQuery = 'SELECT * FROM public.ram_usage WHERE date = $1';
	const updateUsageQuery = `UPDATE public.ram_usage
        SET usage=$2 WHERE date=$1 returning id, usage, to_char(date, 'YYYY-MM-DD HH24:MI:SS TZ') as date`;
	try {
		const { rows } = await dbQuery.query(findUsageQuery, [dateQuery]);
		const dbResponse = rows[0];
		if (!dbResponse) {
			errorMessage.error = 'Usage Cannot be found';
			return res.status(status.notfound).send(errorMessage);
		}
		const response = await dbQuery.query(updateUsageQuery, [dateQuery, usage]);
		const dbResult = response.rows[0];
		delete dbResult.password;
		successMessage.data = dbResult;
		return res.status(status.success).send(successMessage);
	} catch (error) {
		return res.status(status.error).send(errorMessage);
	}
}

export {
	getRamUsage,
	updateRamUsage
}
