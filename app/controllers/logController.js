import dbQuery from '../db/dev/dbQuery';

import {
	errorMessage, successMessage, status,
} from '../helpers/status';
import moment from "moment";

const getLogList = async (req, res) => {
	const start = `'${moment(new Date()).add(-2, 'hour').format()}'`
	const end = `'${moment(new Date()).format()}'`

	const getLogListloadQuery = `SELECT * FROM public.log_list WHERE date >= ${start} AND date <= ${end}`;
	try {
		const { rows } = await dbQuery.query(getLogListloadQuery);
		const dbResponse = rows;
		if (dbResponse[0] === undefined) {
			errorMessage.error = 'There are no logs';
			return res.status(status.bad).send(errorMessage);
		}
		successMessage.data = dbResponse;
		return res.status(status.success).send(successMessage);
	} catch (error) {
		errorMessage.error = 'An error Occured';
		return res.status(status.error).send(errorMessage);
	}
}


export {
	getLogList
}
