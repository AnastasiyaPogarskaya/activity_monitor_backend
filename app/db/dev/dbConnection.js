import pool from './pool';

pool.on('connect', () => {
  console.log('connected to the db');
});

/**
 * Create Cores Table
 */
const createCpuCoresTable = () => {
  const cpuCoresCreateQuery = `CREATE TABLE IF NOT EXISTS cpu_cores
  (id SERIAL PRIMARY KEY,
  cpu_core VARCHAR(100) NOT NULL, 
  usage integer)`;

  pool.query(cpuCoresCreateQuery)
    .then((res) => {
      console.log(res);
      pool.end();
    })
    .catch((err) => {
      console.log(err);
      pool.end();
    });
};

/**
 * Create Log Table
 */
const createLogTable = () => {
    const logCreateQuery = `CREATE TABLE IF NOT EXISTS log_list
  (id SERIAL PRIMARY KEY,
  log text NOT NULL, 
  date timestamp with time zone NOT NULL)`;

    pool.query(logCreateQuery)
        .then((res) => {
            console.log(res);
            pool.end();
        })
        .catch((err) => {
            console.log(err);
            pool.end();
        });
};

/**
 * Create CPU workload
 */
const createCpuUsageTable = () => {
  const busCreateQuery = `CREATE TABLE IF NOT EXISTS cpu_usage
    (id SERIAL PRIMARY KEY,
    usage integer,
    date timestamp with time zone NOT NULL)`;

  pool.query(busCreateQuery)
    .then((res) => {
      console.log(res);
      pool.end();
    })
    .catch((err) => {
      console.log(err);
      pool.end();
    });
};

/**
 * Create RAM workload
 */
const createRamUsageTable = () => {
    const busCreateQuery = `CREATE TABLE IF NOT EXISTS ram_usage
    (id SERIAL PRIMARY KEY,
    usage integer,
    date timestamp with time zone NOT NULL)`;

    pool.query(busCreateQuery)
        .then((res) => {
            console.log(res);
            pool.end();
        })
        .catch((err) => {
            console.log(err);
            pool.end();
        });
};

/**
 * Drop Cores Table
 */
const dropCpuCoresTable = () => {
  const usersDropQuery = 'DROP TABLE IF EXISTS cpu_cores';
  pool.query(usersDropQuery)
    .then((res) => {
      console.log(res);
      pool.end();
    })
    .catch((err) => {
      console.log(err);
      pool.end();
    });
};

/**
 * Drop Log Table
 */
const dropLogTable = () => {
  const usersDropQuery = 'DROP TABLE IF EXISTS log_list';
  pool.query(usersDropQuery)
    .then((res) => {
      console.log(res);
      pool.end();
    })
    .catch((err) => {
      console.log(err);
      pool.end();
    });
};

/**
 * Drop CPU usage Table
 */
const dropCpuUsageTable = () => {
    const usersDropQuery = 'DROP TABLE IF EXISTS cpu_usage';
    pool.query(usersDropQuery)
        .then((res) => {
            console.log(res);
            pool.end();
        })
        .catch((err) => {
            console.log(err);
            pool.end();
        });
};

/**
 * Drop Ram usage Table
 */
const dropRamUsageTable = () => {
    const usersDropQuery = 'DROP TABLE IF EXISTS ram_usage';
    pool.query(usersDropQuery)
        .then((res) => {
            console.log(res);
            pool.end();
        })
        .catch((err) => {
            console.log(err);
            pool.end();
        });
};


/**
 * Create All Tables
 */
const createAllTables = () => {
  createCpuCoresTable();
  createCpuUsageTable();
  createRamUsageTable();
  createLogTable();
};


/**
 * Drop All Tables
 */
const dropAllTables = () => {
    dropCpuCoresTable()
    dropCpuUsageTable()
    dropRamUsageTable()
    dropLogTable()
};

pool.on('remove', () => {
  console.log('client removed');
  process.exit(0);
});


export {
  createAllTables,
  dropAllTables,
};

require('make-runnable');
